package org.icommerce.search.spring;

import org.icommerce.search.models.Product;
import org.icommerce.search.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Product("book-1", "book1", BigDecimal.valueOf(10))));
            log.info("Preloading " + repository.save(new Product("book-2","book2", BigDecimal.valueOf(15))));
        };
    }
}