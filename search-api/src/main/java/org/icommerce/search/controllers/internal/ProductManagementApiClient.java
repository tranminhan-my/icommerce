package org.icommerce.search.controllers.internal;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "product-management-api")
public interface ProductManagementApiClient {
    @RequestMapping(method = RequestMethod.GET, value = "/products")
    List<ProductDTO> getProducts();
}
