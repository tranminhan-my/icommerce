package org.icommerce.search.controllers.internal;

import org.icommerce.search.models.Product;
import org.icommerce.search.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/internal")
public class IndexController {
    ProductRepository repository;
    ProductManagementApiClient apiClient;

    private static final Logger log = LoggerFactory.getLogger(IndexController.class);

    public IndexController(ProductRepository repository, ProductManagementApiClient apiClient) {
        this.repository = repository;
        this.apiClient = apiClient;
    }

    @PostMapping("/refresh-all")
    public ResponseEntity<?> refreshAll() {
        this.repository.deleteAll();

        List<ProductDTO> products = apiClient.getProducts();
        products.stream().forEach(p -> {
            Product product = new Product();
            product.setId(p.getId().toString());
            product.setName(p.getName());
            product.setPrice(p.getPrice());
            product.setBrand(p.getBrand());
            product.setCategory(p.getCategory());
            product.setColor(p.getColor());

            this.repository.save(product);
        });

        return ResponseEntity.ok("Done");
    }
}