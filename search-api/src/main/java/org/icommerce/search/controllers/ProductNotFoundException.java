package org.icommerce.search.controllers;

public class ProductNotFoundException extends RuntimeException {
    ProductNotFoundException(String name) {
        super("Could not find product " + name);
    }
}
