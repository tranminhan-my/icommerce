package org.icommerce.search.controllers;

import org.icommerce.search.assemblers.ProductAssembler;
import org.icommerce.search.models.Product;
import org.icommerce.search.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.fuzzyQuery;
import static org.elasticsearch.index.query.QueryBuilders.regexpQuery;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class SearchController {
    ProductRepository repository;
    ProductAssembler assembler;
    ElasticsearchOperations elasticsearchTemplate;

    private static final Logger log = LoggerFactory.getLogger(SearchController.class);

    public SearchController(ProductRepository repository, ProductAssembler assembler, ElasticsearchOperations elasticsearchTemplate) {
        this.repository = repository;
        this.assembler = assembler;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @GetMapping("/products/{name}")
    public EntityModel<Product> find(@PathVariable String name) {
        log.info("get search request for: " + name);
        Page<Product> byName = repository.findByName(name, PageRequest.of(0, 10));

        EntityModel<Product> productEntityModel = byName.stream()
                .findFirst()
                .map(assembler::toModel)
                .orElseThrow(() -> new ProductNotFoundException(name));

        return productEntityModel;
    }

    @GetMapping("/search")
    public CollectionModel<EntityModel<Product>> search(@RequestParam String term) {
        log.info("get search request for: " + term);

        Query searchQuery = new NativeSearchQueryBuilder()
                .withFilter(fuzzyQuery("name", term))
                .build();
        SearchHits<Product> articles =
                elasticsearchTemplate.search(searchQuery, Product.class, IndexCoordinates.of("products"));

        List<EntityModel<Product>> products = articles.stream()
                .map(productSearchHit -> assembler.toModel(productSearchHit.getContent()))
                .collect(Collectors.toList());

        return CollectionModel.of(products,
                linkTo(methodOn(SearchController.class).search(term)).withSelfRel());
    }
}
