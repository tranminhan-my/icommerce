package org.icommerce.assemblers;

import org.icommerce.controllers.ProductController;
import org.icommerce.models.Product;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class ProductAssembler implements RepresentationModelAssembler<Product, EntityModel<Product>> {
    @Override
    public EntityModel<Product> toModel(Product product) {

        return EntityModel.of(product, //
            linkTo(methodOn(ProductController.class).find(product.getId())).withSelfRel()
        );
    }
}
