package org.icommerce.controllers;

import org.icommerce.assemblers.ProductAssembler;
import org.icommerce.models.Product;
import org.icommerce.repositories.ProductRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ProductController {
    ProductRepository repository;
    ProductAssembler assembler;

    public ProductController(ProductRepository repository, ProductAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/products/{id}")
    public EntityModel<Product> find(@PathVariable Long id) {
        Product product = repository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));

        return assembler.toModel(product);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<?> update(@RequestBody Product newProduct, @PathVariable Long id) {
        Product updatedProduct = repository.findById(id) //
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setPrice(newProduct.getPrice());
                    return repository.save(product);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return repository.save(newProduct);
                });

        EntityModel<Product> productModel = assembler.toModel(updatedProduct);

        return ResponseEntity
                .created(productModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(productModel);
    }

    @GetMapping("/products")
    public ResponseEntity<?> all() {
        List<Product> products = repository.findAll().stream()
                .collect(Collectors.toList());

        return ResponseEntity.of(Optional.of(products));
    }
}
