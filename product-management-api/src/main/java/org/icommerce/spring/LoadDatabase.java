package org.icommerce.spring;

import org.icommerce.models.Product;
import org.icommerce.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Product("book 1", BigDecimal.valueOf(10))));
            log.info("Preloading " + repository.save(new Product("book 2", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 3", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 4", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 5", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 6", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 7", BigDecimal.valueOf(15))));
            log.info("Preloading " + repository.save(new Product("book 8", BigDecimal.valueOf(15))));
        };
    }
}