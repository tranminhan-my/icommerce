# Installation instructions
To start services, run following snippets

```
./build.sh
docker-compose up
```

It will build and start 4 services. Visit [http://localhost:8761/](http://localhost:8761/) to see the status of the Eureka discovery server.
![Startup Status](https://dl.dropboxusercontent.com/s/j17imb96jcq3j9i/Screen_Shot_2020-08-17_at_1.29.08_AM.png?dl=0 "Startup Status")

Wait for a few minutes for all services to get to the stable status then run the testing steps below.

## Testing

1. Search
```
curl localhost:8080/search/search?term=book
```
It should return `2` products as below

![Search Result](https://dl.dropboxusercontent.com/s/cbtnmjv9z1lt7jt/Screen_Shot_2020-08-17_at_1.30.19_AM.png?dl=0 "Search Result")

2. Reindex search
```
curl -X POST localhost:8080/search/internal/refresh-all
curl localhost:8080/search/search?term=book 
```

It should return `8` products as below
![Search Result](https://dl.dropboxusercontent.com/s/2sh4onn4z39nozv/Screen_Shot_2020-08-17_at_1.52.44_AM.png?dl=0 "Search Result")

This is because the previous operation will clean the current elastic search index and re-populate again all products 
from the product-management-api database.

### System Design
The system consists of multiple components as below

![System Design](https://dl.dropboxusercontent.com/s/uiyy4s9mfxipixr/icommerce-icommerce.png?dl=0 "System Design")

Items in `orange` are covered in this submission. 
Items in `gray` are NOT covered in this submission.

- `Front-end App`: interacts with various backend services
- `Search API`: In the scope of this submission, I draft a simple implementation 
and didn't support filtering by colors, etc.
```
search(term, categories, brands, colors, prices, sort)
```

- `Order Service`: to add products to cart and place an order 

To support those customer facing services, there are some supporting services

- `API Gateway`: to simplify access to various backend APIs 
- `Identity & Access Management Service`: such service is big and time-consuming to implement, so it's not covered. Its 
main role is to ensure users and services are authenticated and authorized properly before calling any downstream services.
- `Product Management Service`: to allow the company to update product prices and other information.
- `Discovery Service`: to allow each service to discover the location of service instances, without keeping their static URLs.

These services are designed as microservices using some patterns 
- Each service has its own database. They interact with each other via APIs. See more at https://microservices.io/patterns/data/database-per-service.html
- Front-End client interacts with services through a gateway to reduce the complexities from the client. See more at https://microservices.io/patterns/apigateway.html
- These components are loosely couple, allows each component to evolve at different speed. 

### Folder Structure
The parent folder contains 4 sub-folders. Each sub-folder is a micro-service using Spring Boot.

| service                        | folder                 |
|--------------------------------|------------------------|
| Discovery Service              | discovery-server       |
| API Gateway                    | gateway                |
| Product Catalog Search Service | search-api             |
| Product Management Service     | product-management-api |  

### Database Design
I take a simplistic approach to database design. To serve a shopping application, the requirement can go rather complex. 
To illustrate the point, I only keep one table 
`products` (there many be tons of other supporting tables like sku, product attributes, etc - 
see [BroadleafCommerce](https://github.com/BroadleafCommerce/BroadleafCommerce/tree/develop-6.0.x/core/broadleaf-framework/src/main/java/org/broadleafcommerce/core/catalog/domain) as a real world example)

# Technologies used
Use standard technologies 
- Java 8
- Spring / JPA / Spring Cloud / Spring Boot
