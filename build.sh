#!/bin/bash

echo "Performing a clean Maven build"
./mvnw clean package -DskipTests=true

for dir in */
do
  (
  cd "$dir" || exit
  tag_name="${dir//\/}"
  docker build --tag "$tag_name":latest .
  )
done
